#!/bin/bash

## This script is executed before the CODECO operator undeployment starts. It is used to remove other components
## that depend on the CODECO operator.

echo "Executing pre undeployment tasks..."

##TODO(user): Add your pre undeployment tasks here
